import './style/index.css';
import { render, h } from 'preact';
import App from './components/app';

render(h(App), document.body);